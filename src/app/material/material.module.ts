import { NgModule } from '@angular/core';
import {
    MatButtonModule, MatCardModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule,
    MatMenuModule, MatRadioModule, MatSelectModule, MatToolbarModule
} from '@angular/material';

@NgModule({
    imports: [
        MatButtonModule, MatCheckboxModule, MatMenuModule, MatIconModule, MatCardModule,
        MatInputModule, MatFormFieldModule, MatSelectModule, MatToolbarModule, MatRadioModule
    ],
    exports: [
        MatButtonModule, MatCheckboxModule, MatMenuModule, MatIconModule, MatCardModule,
        MatInputModule, MatFormFieldModule, MatSelectModule, MatToolbarModule, MatRadioModule
    ],
    declarations: []
})
export class MaterialModule {
}
