import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { AuthModule } from './auth/auth.module';
import { RouterModule } from '@angular/router';
import { routes } from './app.routing';
import { MenuComponent } from './common/menu/menu.component';

import { StoreModule } from '@ngrx/store';
import { TrainingsModule } from './trainings/trainings.module';

import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { CustomRouterStateSerializer } from './store/utils';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { reducers, metaReducers } from './store/root.reducers';
import { environment } from '../environments/environment';
import { ErrorsModule } from './errors/errors.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppHttpInterceptor } from './http.interceptor';
import { GlobalErrorHandler } from './error.handler';

@NgModule({
    declarations: [
        AppComponent,
        MenuComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,

        AuthModule,
        TrainingsModule,
        ErrorsModule,

        RouterModule.forRoot(routes, {useHash: true}),

        StoreModule.forRoot(reducers, {metaReducers}),

        StoreRouterConnectingModule,
        !environment.production ? StoreDevtoolsModule.instrument() : [],
        EffectsModule.forRoot([])
    ],
    providers: [
        {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer},
        {provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true},
        {provide: ErrorHandler, useClass: GlobalErrorHandler}
    ],
    bootstrap: [AppComponent],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class AppModule {
}
