import * as fromRouter from '@ngrx/router-store';
import { MetaReducer } from '@ngrx/store';
import { RouterStateUrl } from './utils';
import { environment } from '../../environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';
import { IAuthState } from '../auth/store/auth.reducers';
import { RouterStateSnapshot } from '@angular/router';
import { IErrorsState } from '../errors/store/errors.reducers';

export interface AppState {
    routerReducer: fromRouter.RouterReducerState<RouterStateSnapshot>;
    auth?: IAuthState,
    errors?: IErrorsState
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production
    ? [storeFreeze]
    : [];

export const reducers = {
    routerReducer: fromRouter.routerReducer
};