/**
 * That is a place where all selectors from modules should be exported to.
 * This approach should avoid from modules knowing each other existence.
 *
 */

export * from './../auth/store/selectors';