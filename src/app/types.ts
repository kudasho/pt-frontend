export interface IUser {
    email: string;
}

export class User implements IUser {
    email: string;
}

export interface IProgram {
    title: string;
    description: string;
    user?: IUser;
}

export class Program implements IProgram {
    title: string;
    description: string;
}