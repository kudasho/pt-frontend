import { Component, Input } from '@angular/core';
import { IProgram, Program } from '../../../types';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-program-create',
    templateUrl: './program-create.component.html',
    styleUrls: ['./program-create.component.scss']
})
export class ProgramCreateComponent {
    @Input() program: IProgram = new Program();

    protected programCreateForm: FormGroup;

    constructor(private fb: FormBuilder) {
        this.createForm();
    }

    createForm() {
        this.programCreateForm = this.fb.group({
            title: [this.program.title, Validators.required],
            description: [this.program.description]
        });
    }

    createProgram() {
        if (!this.programCreateForm.valid) {
            throw new Error('form is not valid');
        }

        console.log(this.programCreateForm.value);
    }
}