import { Component, Input } from '@angular/core';
import { IProgram, Program } from '../../../types';

@Component({
    selector: 'app-program-item',
    templateUrl: './program-item.component.html',
    styleUrls: ['./program-item.component.scss']
})
export class ProgramItemComponent {
    @Input()
    program: IProgram = new Program();
}