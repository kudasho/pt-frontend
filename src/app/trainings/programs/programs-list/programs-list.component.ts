import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ITrainingsState } from '../../store/trainings.reducers';
import { Observable } from 'rxjs/Observable';
import { IProgram, Program } from '../../../types';
import { selectProgramsList } from '../../store/trainings.selectors';
import { GetProgramsListAction } from '../../store/trainings.actions';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-programs-list',
    templateUrl: './programs-list.component.html',
    styleUrls: ['./programs-list.component.scss']
})
export class ProgramsListComponent implements OnInit {
    programs$: Observable<IProgram[]>;

    constructor(protected store: Store<ITrainingsState>, protected router: Router, private activatedRoute: ActivatedRoute) {
        this.programs$ = store.select(selectProgramsList);
    }

    ngOnInit() {
        return this.store.dispatch(new GetProgramsListAction());
    }

    createProgram() {
        return this.router.navigate(['../create'], {relativeTo: this.activatedRoute});
    }
}
