import { Routes } from '@angular/router';
import { AuthGuard } from '../auth/services/auth-guard.service';
import { ProgramsListComponent } from './programs/programs-list/programs-list.component';
import { ProgramCreateComponent } from './programs/program-create/program-create.component';

export const routes: Routes = [
    {
        path: 'trainings',
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                redirectTo: 'programs/list',
                pathMatch: 'full'
            },

            {
                path: 'programs',
                children: [
                    {
                        path: 'list',
                        component: ProgramsListComponent
                    },
                    {
                        path: 'create',
                        component: ProgramCreateComponent
                    }
                ]
            }
        ]
    }

];