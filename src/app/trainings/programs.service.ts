import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClient } from '@angular/common/http';
import { IProgram } from '../types';


@Injectable()
export class ProgramsService {

    constructor(private http: HttpClient) {
    }

    getProgramsList(): Observable<IProgram[]> {
        return this.http.get<IProgram[]>(
            '/api/programs'
        );
    }

    createProgram(data: IProgram): Observable<IProgram> {
        return this.http.post<IProgram>(
            '/api/programs',
            data
        );
    };

    showProgram(programId: number): Observable<IProgram> {
        return this.http.get<IProgram>(
            `/api/programs/${programId}`
        );
    }

    updateProgram(programId: number, data: IProgram): Observable<IProgram> {
        return this.http.put<IProgram>(
            `/api/programs/${programId}`,
            data
        );
    }

    deleteProgramAction(programId: number): Observable<boolean> {
        return this.http.delete<boolean>(
            `/api/programs/${programId}`
        );
    }
}
