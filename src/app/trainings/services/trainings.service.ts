import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClient } from '@angular/common/http';
import { IProgram } from '../../types';


@Injectable()
export class TrainingsService {

    constructor(private http: HttpClient) {
    }

    getProgramsList(): Observable<IProgram[]> {
        return this.http.get<IProgram[]>(
            '/api/trainings'
        );
    }
}
