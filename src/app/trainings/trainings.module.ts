import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { routes } from './trainings.routing';
import { TrainingsListComponent } from './list/list.component';
import { TrainingsService } from './services/trainings.service';
import { trainingsReducer } from './store/trainings.reducers';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TrainingsEffectsService } from './store/trainings.effects';
import { ProgramsListComponent } from './programs/programs-list/programs-list.component';
import { ProgramItemComponent } from './programs/program-item/program-item.component';
import { MaterialModule } from '../material/material.module';
import { ProgramsService } from './programs.service';
import { ProgramCreateComponent } from './programs/program-create/program-create.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),

        MaterialModule,

        StoreModule.forFeature('trainings', trainingsReducer),
        EffectsModule.forFeature([
            TrainingsEffectsService
        ])
    ],
    declarations: [
        TrainingsListComponent,
        ProgramsListComponent,
        ProgramItemComponent,
        ProgramCreateComponent
    ],
    providers: [
        TrainingsService,
        ProgramsService
    ],

    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class TrainingsModule {
}
