import { ITrainingsState } from './trainings.reducers';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const selectTrainingsState = createFeatureSelector<ITrainingsState>('trainings');

export const selectProgramsList = createSelector(
    selectTrainingsState,
    (state: ITrainingsState) => state.programs
);
