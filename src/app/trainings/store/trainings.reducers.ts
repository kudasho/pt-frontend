import { Action } from '@ngrx/store';
import { IProgram } from '../../types';
import * as _ from 'lodash';
import { PROGRAMS_LIST_RECEIVED_ACTION } from './trainings.actions';

export interface ITrainingsState {
    programs: IProgram[];
}

export const INITIAL_TRAININGS_STATE: ITrainingsState = {
    programs: []
};

export function trainingsReducer(state: ITrainingsState = INITIAL_TRAININGS_STATE, action): ITrainingsState {
    let newState = _.cloneDeep(state);

    switch (action.type) {
        case PROGRAMS_LIST_RECEIVED_ACTION: {
            newState.programs = action.payload;

            return newState;
        }

        default:
            return newState;
    }
}