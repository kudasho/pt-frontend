import { Action } from '@ngrx/store';
import { IProgram } from '../../types';

export const GET_PROGRAMS_LIST_ACTION = '[trainings] GET_PROGRAMS_LIST_ACTION';
export const PROGRAMS_LIST_RECEIVED_ACTION = '[trainings] PROGRAMS_LIST_RECEIVED_ACTION';

export class GetProgramsListAction implements Action {
    readonly type = GET_PROGRAMS_LIST_ACTION;

    constructor() {
    }
}

export class ProgramsListReceived implements Action {
    readonly type = PROGRAMS_LIST_RECEIVED_ACTION;

    constructor(public payload: IProgram[]) {
    }
}