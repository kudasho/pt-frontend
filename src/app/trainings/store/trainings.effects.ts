import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ProgramsService } from '../programs.service';
import { ITrainingsState } from './trainings.reducers';
import { GET_PROGRAMS_LIST_ACTION, GetProgramsListAction, ProgramsListReceived } from './trainings.actions';

@Injectable()
export class TrainingsEffectsService {

    constructor(private actions$: Actions, protected programsService: ProgramsService,
                protected store: Store<ITrainingsState>) {

    }

    @Effect()
    getPrograms$ = this.actions$
        .ofType<GetProgramsListAction>(GET_PROGRAMS_LIST_ACTION)
        .switchMap(() => this.programsService.getProgramsList()
            .map(list => new ProgramsListReceived(list))
        );
}