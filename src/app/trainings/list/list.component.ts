import { Component, OnInit } from '@angular/core';
import { ITrainingsState } from '../store/trainings.reducers';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-trainings-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class TrainingsListComponent implements OnInit {

    constructor(protected store: Store<ITrainingsState>) {
    }

    ngOnInit() {
    }

}
