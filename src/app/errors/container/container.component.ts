import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { IErrorsState } from '../store/errors.reducers';
import { selectErrorsList } from '../store/selectors';
import { AppError } from '../types';
import { ErrorsAcknowledgedAction } from '../store/errors.actions';

@Component({
    selector: 'app-errors-container',
    templateUrl: './container.component.html',
    styleUrls: ['./container.component.css']
})
export class ErrorsContainerComponent implements OnInit {
    errors$: Observable<AppError[]>;

    constructor(private store: Store<IErrorsState>) {
        this.errors$ = store.select(selectErrorsList);
    }

    ngOnInit() {
    }

    ackErrors() {
        this.store.dispatch(new ErrorsAcknowledgedAction());
    }
}
