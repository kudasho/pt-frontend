export class AppError {
    message: string;
    code?: number;
}