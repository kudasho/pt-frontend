import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { ErrorsContainerComponent } from './container/container.component';
import { StoreModule } from '@ngrx/store';
import { errorsReducer } from './store/errors.reducers';
import { EffectsModule } from '@ngrx/effects';
import { ErrorsEffectsService } from './store/errors.effects';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,

        StoreModule.forFeature('errors', errorsReducer),
        EffectsModule.forFeature([
            ErrorsEffectsService
        ])
    ],

    declarations: [
        ErrorsContainerComponent
    ],

    exports: [
        ErrorsContainerComponent
    ],

    bootstrap: [
        ErrorsContainerComponent
    ]
})
export class ErrorsModule {
}
