import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { NEW_API_ERROR_OCCURRED_ACTION, NewApiErrorOccurredAction } from './errors.actions';
import { Store } from '@ngrx/store';
import { IErrorsState } from './errors.reducers';


@Injectable()
export class ErrorsEffectsService {

    constructor(private actions$: Actions,
                protected store: Store<IErrorsState>) {
    }

}
