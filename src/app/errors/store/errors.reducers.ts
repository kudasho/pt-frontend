import { ERRORS_ACKNOWLEDGED_ACTION, NEW_API_ERROR_OCCURRED_ACTION, NEW_ERROR_OCCURRED_ACTION } from './errors.actions';
import { AppError } from '../types';
import * as _ from 'lodash';

export interface IErrorsState {
    errors: AppError[];
}

export const INITIAL_ERRORS_STATE: IErrorsState = {
    errors: []
};

export function errorsReducer(state: IErrorsState = INITIAL_ERRORS_STATE, action: any): IErrorsState {
    let newState = _.cloneDeep(state);

    switch (action.type) {
        case NEW_ERROR_OCCURRED_ACTION:
            newState.errors = state.errors.concat([action.payload]);

            return newState;
        case NEW_API_ERROR_OCCURRED_ACTION:
            newState.errors = state.errors.concat([action.payload]);

            return newState;

        case ERRORS_ACKNOWLEDGED_ACTION:
            newState.errors = [];

            return newState;

        default:
            return newState;
    }
}