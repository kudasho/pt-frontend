import { Action } from '@ngrx/store';
import { AppError } from '../types';

export const NEW_ERROR_OCCURRED_ACTION = '[errors] NEW_ERROR_OCCURRED_ACTION';
export const NEW_API_ERROR_OCCURRED_ACTION = '[errors] NEW_API_ERROR_OCCURRED_ACTION';
export const NOT_AUTHENTICATED_ERROR_OCCURRED_ACTION = '[errors] NOT_AUTHENTICATED_ERROR_OCCURRED_ACTION';
export const ERRORS_ACKNOWLEDGED_ACTION = '[errors] ERRORS_ACKNOWLEDGED_ACTION';

export class NewApiErrorOccurredAction implements Action {
    readonly type = NEW_API_ERROR_OCCURRED_ACTION;

    constructor(public payload: AppError) {

    }
}

export class NewErrorOccurredAction implements Action {
    readonly type = NEW_ERROR_OCCURRED_ACTION;

    constructor(public payload: Error) {

    }
}

export class NotAuthenticationErrorOccuredAction implements Action {
    readonly type = NOT_AUTHENTICATED_ERROR_OCCURRED_ACTION;
}

export class ErrorsAcknowledgedAction implements Action {
    readonly type = ERRORS_ACKNOWLEDGED_ACTION;

    constructor() {

    }
}