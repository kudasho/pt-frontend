import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IErrorsState } from './errors.reducers';

export const selectErrorsState = createFeatureSelector<IErrorsState>('errors');


export const selectErrorsList = createSelector(
    selectErrorsState,
    (state: IErrorsState) => state.errors
);