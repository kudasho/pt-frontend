import { IAuthState } from './auth.reducers';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const selectAuthState = createFeatureSelector<IAuthState>('auth');

export const selectIsAuthenticated = createSelector(
    selectAuthState,
    (state: IAuthState) => state.isAuthenticated
);

export const selectAuthUser = createSelector(
    selectAuthState,
    (state: IAuthState) => state.authUser
);

export const selectRedirectAfterLoginTo = createSelector(
    selectAuthState,
    (state: IAuthState) => state.redirectAfterLoginTo
);