import { Action } from '@ngrx/store';
import { IUser } from '../../types';

export const LOG_IN_SUCCESS_ACTION = '[auth] LOG_IN_USER_ACTION';
export const LOG_IN_ATTEMPT_ACTION = '[auth] LOG_IN_ATTEMPT_ACTION';
export const REDIRECT_TO_LOG_IN_ACTION = '[auth] REDIRECT_TO_LOG_IN_ACTION';
export const REGISTER_ATTEMPT_ACTION = '[auth] REGISTER_ATTEMPT_ACTION';
export const LOAD_AUTH_USER_ACTION = '[auth] LOAD_AUTH_USER_ACTION';
export const LOG_OUT_ACTION = '[auth] LOG_OUT_ACTION';

export class LogInSuccessAction implements Action {
    readonly type = LOG_IN_SUCCESS_ACTION;

    constructor(public payload: IUser) {

    }
}

export class LogInAttemptAction implements Action {
    readonly type = LOG_IN_ATTEMPT_ACTION;

    constructor(public payload: { email: string, password: string }) {

    }
}

export class RegisterAttemptAction implements Action {
    readonly type = REGISTER_ATTEMPT_ACTION;

    constructor(public payload: IRegisterData) {

    }
}

export class RedirectToLogInAction implements Action {
    readonly type = REDIRECT_TO_LOG_IN_ACTION;

    constructor(public payload?: string) {

    }
}

export class LoadAuthUserAction implements Action {
    readonly type = LOAD_AUTH_USER_ACTION;

    constructor(public payload?: string) {
    }
}

export class LogOutAction implements Action {
    readonly type = LOG_OUT_ACTION;
}
