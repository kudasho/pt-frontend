import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

import {
    LOG_IN_ATTEMPT_ACTION, REDIRECT_TO_LOG_IN_ACTION, LogInAttemptAction, RedirectToLogInAction,
    LogInSuccessAction, LOG_IN_SUCCESS_ACTION, RegisterAttemptAction, REGISTER_ATTEMPT_ACTION, LoadAuthUserAction,
    LOAD_AUTH_USER_ACTION, LogOutAction, LOG_OUT_ACTION
} from './auth.actions';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { IAuthState } from './auth.reducers';
import { Store } from '@ngrx/store';
import { selectRedirectAfterLoginTo } from './selectors';
import { NEW_API_ERROR_OCCURRED_ACTION, NewApiErrorOccurredAction } from '../../errors/store/errors.actions';


@Injectable()
export class AuthEffectsService {

    constructor(private actions$: Actions, protected authService: AuthService,
        protected store: Store<IAuthState>,
        private router: Router) {
    }


    @Effect() authAttempt$: Observable<any> = this.actions$
        .ofType<LogInAttemptAction>(LOG_IN_ATTEMPT_ACTION)
        .switchMap(action => this.authService.login(action.payload.email, action.payload.password))
        .map(authedUser => new LogInSuccessAction(authedUser));

    @Effect() registerAttempt$: Observable<any> = this.actions$
        .ofType<RegisterAttemptAction>(REGISTER_ATTEMPT_ACTION)
        .switchMap(action => this.authService.register(action.payload)
            .map(authedUser => new LogInSuccessAction(authedUser))
        );

    @Effect({ dispatch: false })
    loginRedirect$ = this.actions$
        .ofType<RedirectToLogInAction>(REDIRECT_TO_LOG_IN_ACTION)
        .do(params => {
            return this.router.navigate(['/auth/login']);
        });


    @Effect({ dispatch: false })
    loginSuccess$ = this.actions$
        .ofType<LogInSuccessAction>(LOG_IN_SUCCESS_ACTION)
        .switchMap(() => this.store.select(selectRedirectAfterLoginTo))
        .map(navigateTo => {
            // todo prevent route cancel at guard, find some solution
            if (!navigateTo || navigateTo === '/auth/login') {
                navigateTo = '/trainings';
            }

            return this.router.navigate([navigateTo]);
        });

    @Effect()
    authUser$ = this.actions$
        .ofType<LoadAuthUserAction>(LOAD_AUTH_USER_ACTION)
        .switchMap(() => this.authService.getMe()
            .map(authedUser => new LogInSuccessAction(authedUser))
        );

    @Effect()
    errorOccurred$: Observable<any> = this.actions$
        .ofType<NewApiErrorOccurredAction>(NEW_API_ERROR_OCCURRED_ACTION)
        .map(action => {
            if (action.payload.code === 401) {
                return new RedirectToLogInAction();
            }

            return Observable.of(action);
        });

    @Effect({ dispatch: false })
    logout$ = this.actions$
        .ofType<LogOutAction>(LOG_OUT_ACTION)
        .switchMap(() => this.authService.logout())
        .map(() => this.router.navigate(['/auth', 'login']))

}
