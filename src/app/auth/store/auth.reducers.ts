import {
    LOG_IN_ATTEMPT_ACTION, LOG_IN_SUCCESS_ACTION, LOG_OUT_ACTION, REDIRECT_TO_LOG_IN_ACTION,
    REGISTER_ATTEMPT_ACTION
} from './auth.actions';
import { IUser } from '../../types';

import * as _ from 'lodash';


export interface IAuthState {
    authUser: IUser;
    isAuthenticated: boolean;
    redirectAfterLoginTo: string;
}

export const INITIAL_AUTH_STATE: IAuthState = {
    authUser: undefined,
    isAuthenticated: false,
    redirectAfterLoginTo: undefined
};

export function authReducer(state: IAuthState = INITIAL_AUTH_STATE, action: any): IAuthState {
    let newState = _.cloneDeep(state);

    switch (action.type) {
        case LOG_IN_SUCCESS_ACTION:

            newState.authUser = action.payload;
            newState.isAuthenticated = true;

            return newState;


        case LOG_IN_ATTEMPT_ACTION:
            return newState;

        case REGISTER_ATTEMPT_ACTION:
            return newState;


        case REDIRECT_TO_LOG_IN_ACTION:
            newState.redirectAfterLoginTo = action.payload;

            return newState;

        case LOG_OUT_ACTION:
            newState.isAuthenticated = false;
            newState.authUser = undefined;

            return newState;

        default:
            return newState;
    }
}