

interface IRegisterData {
    email: string;
    password: string;
    password_confirmation: string;
}