// import { Injectable } from '@angular/core';
// import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
// import { AuthService } from '../services/auth.service';
// import { Store } from '@ngrx/store';
// import { IAuthState } from '../store/auth.reducers';
// import { LogInSuccessAction } from '../store/auth.actions';
//
// @Injectable()
// export class AuthResolver implements Resolve<{}> {
//     constructor(protected authService: AuthService,
//                 protected store: Store<IAuthState>,
//                 protected router: Router) {
//     }
//
//     resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
//         return this.load(route, state);
//     }
//
//     async load(route, state) {
//         return this.authService.getMe()
//             .map(authUser => {
//                 this.store.dispatch(new LogInSuccessAction(authUser));
//             });
//     }
// }
