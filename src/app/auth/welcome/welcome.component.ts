import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
// import {AppState, getAuthState} from "../../store";

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
    protected auth$;
    protected sub;

    constructor() {
        // this.auth$ = store.select(getAuthState);
    }

    ngOnInit() {
        // this.sub = this.auth$.subscribe((res: IAuthState) => {
        //     console.log(res);
        // })
    }

    login() {
        // this.store.dispatch(new AuthAction('Alex'));
    }

    logout() {
        // this.store.dispatch(new LogoutAction('guest'));
    }

}
