import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormsErrorStateMatchet } from '../../common/forms/errorStateMatcher';
import { PasswordValidation } from '../../common/forms/passwordConfirm.validator';
import { Store } from '@ngrx/store';
import { IAuthState } from '../store/auth.reducers';
import { RegisterAttemptAction } from '../store/auth.actions';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    protected registerForm: FormGroup;
    protected matcher = new FormsErrorStateMatchet();


    constructor(protected router: Router, protected fb: FormBuilder, protected store: Store<IAuthState>) {
    }

    ngOnInit() {
        this.createForm();
    }

    createForm() {
        this.registerForm = this.fb.group({
            email: [
                'mos@gmail.com',
                [Validators.email, Validators.required]
            ],
            password: [
                '123123',
                [Validators.required, Validators.minLength(6)]
            ],
            password_confirmation: [
                '123123'
            ],
            role: [
                'trainee',
                Validators.required
            ]
        }, {
            validator: PasswordValidation.matchPassword
        });
    }

    register() {
        return this.store.dispatch(new RegisterAttemptAction(this.registerForm.value));
    }

    goToLogin() {
        return this.router.navigate(['/auth', 'login']);
    }

}
