import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WelcomeComponent } from './welcome/welcome.component';
import { routes } from './auth.routing';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffectsService } from './store/auth.effects';
import { StoreModule } from '@ngrx/store';
import { authReducer } from './store/auth.reducers';
import { AuthGuard } from './services/auth-guard.service';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MaterialModule,
        ReactiveFormsModule,
        HttpClientModule,

        StoreModule.forFeature('auth', authReducer),
        EffectsModule.forFeature([
            AuthEffectsService
        ])
    ],
    declarations: [WelcomeComponent, LoginComponent, RegisterComponent],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    providers: [
        AuthService,
        AuthGuard
    ]
})
export class AuthModule {
}
