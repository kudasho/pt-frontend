import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { IAuthState } from '../store/auth.reducers';
import { selectIsAuthenticated } from '../store/selectors';
import { LoadAuthUserAction, RedirectToLogInAction } from '../store/auth.actions';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private store: Store<IAuthState>) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.store.select(selectIsAuthenticated)
            .map(authed => {
                if (!authed) {
                    this.store.dispatch(new LoadAuthUserAction());

                    return false;
                }

                return true;
            });
    }
}
