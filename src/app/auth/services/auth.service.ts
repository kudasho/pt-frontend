import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClient } from '@angular/common/http';
import { IUser } from '../../types';


@Injectable()
export class AuthService {

    constructor(private http: HttpClient) {
    }

    login(email: string, password: string): Observable<IUser> {
        return this.http.post<IUser>(
            `/api/auth/login`,
            {
                email,
                password
            }
        );
    }

    register(data: IRegisterData): Observable<IUser> {
        return this.http.post<IUser>(`/api/auth/register`, data);
    }

    getMe(): Observable<IUser> {
        return this.http.get<IUser>(
            '/api/auth/me'
        );
    }

    logout(): Observable<boolean> {
        return this.http.post<boolean>('/api/auth/logout', {});
    }
}
