import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { AppState } from '../../store/root.reducers';

import { selectIsAuthenticated, selectAuthUser } from '../../store/selectors';
import { LogOutAction } from '../../auth/store/auth.actions';
import { IUser } from '../../types';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
    authUser$: Observable<IUser>;
    isAuthenticated$: Observable<boolean>;

    constructor(private store: Store<AppState>) {
        this.authUser$ = store.select(selectAuthUser);
        this.isAuthenticated$ = store.select(selectIsAuthenticated);
    }

    ngOnInit() {
    }

    logout() {
        this.store.dispatch(new LogOutAction());
    }
}
