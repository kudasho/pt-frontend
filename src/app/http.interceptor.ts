import { Injectable, Injector } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NewApiErrorOccurredAction } from './errors/store/errors.actions';
import { Store } from '@ngrx/store';
import { IErrorsState } from './errors/store/errors.reducers';
import { AppError } from './errors/types';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

    constructor(private injector: Injector, protected store: Store<IErrorsState>) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const clonedRequest = request.clone({
            // headers: auth.getTokenHeader(),
            // url: this.fixUrl(request.url)
        });

        return next.handle(clonedRequest)
            .catch((err) => this.errorHandler(err));
    }

    errorHandler(err: any) {
        const appError = new AppError();

        appError.message = err.error;
        appError.code = err.status;

        this.store.dispatch(new NewApiErrorOccurredAction(appError));

        return Observable.of(err);
    }
}
