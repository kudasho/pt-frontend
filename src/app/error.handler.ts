import { ErrorHandler, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { IErrorsState } from './errors/store/errors.reducers';
import { NewErrorOccurredAction } from './errors/store/errors.actions';

@Injectable()
export class GlobalErrorHandler extends ErrorHandler implements ErrorHandler {
    constructor(protected store: Store<IErrorsState>) {
        super();
    }

    handleError(error) {
        this.store.dispatch(new NewErrorOccurredAction(new Error(error.message)));

        super.handleError(error);
    }

}